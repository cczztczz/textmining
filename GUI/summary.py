import sys

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from PyQt5 import QtCore

import textrank
import traceback

class MainWidget(QWidget):

    def __init__(self):
        
        super().__init__()

        #시작 위젯 실행
        self.initUI()

    def initUI(self):
        
        #기본 레이아웃 선언 (위에서 아래 -> TopToBottom, 왼쪽에서 오른쪽 -> LeftToRight)
        layout_base=QBoxLayout(QBoxLayout.TopToBottom,self)

        #기본 레이아웃 적용
        self.setLayout(layout_base)

        #첫번째 그룹
        g1 = QGroupBox("Original Text")

        layout_base.addWidget(g1)
    
        layout=QVBoxLayout()

        ot=QTextEdit()
        
        layout.addWidget(ot)

        ot.setMinimumHeight(400)

        ot.zoomIn(0)

        g1.setLayout(layout)

        #두번째 그룹
        g2 = QGroupBox("Summary Text")

        layout_base.addWidget(g2)
    
        layout=QVBoxLayout()

        st=QTextEdit()
        
        layout.addWidget(st)

        st.setMaximumHeight(200)

        st.zoomIn(1)

        st.setAlignment(QtCore.Qt.AlignLeft)

        g2.setLayout(layout)

        #세번째 그룹
        g3 = QGroupBox("Important Text")

        layout_base.addWidget(g3)
    
        layout=QVBoxLayout()

        it=QTextEdit()
        
        layout.addWidget(it)

        it.setMaximumHeight(32)

        it.zoomIn(4)

        g3.setLayout(layout)

        #네번째 그룹
        g4 = QGroupBox()

        layout_base.addWidget(g4)

        layout=QHBoxLayout()

        apply = QPushButton("적용")
        apply.clicked.connect(lambda : self.apply_click(ot, st, it))

        warning = QPushButton("주의사항")
        warning.clicked.connect(self.warning_click)
        
        clear = QPushButton("비우기")
        clear.clicked.connect(lambda : self.clear_click(ot, st, it))

        ex = QPushButton("종료")
        ex.clicked.connect(self.close)
        
        layout.addWidget(apply)
        layout.addWidget(warning)
        layout.addWidget(clear)
        layout.addWidget(ex)

        g4.setLayout(layout)

        #위젯 창 이름
        self.setWindowTitle('Non-literary Summary Program')

        #위젯 시작 위치와 위젯 크기  (x, y, 가로, 세로)
        self.setGeometry(0, 0, 800, 800)

        #center 함수 실행
        self.Gui_Center()
    

    #위젯을 중앙에 위치하게 하는 center 함수 
    def Gui_Center(self):

        qr=self.frameGeometry()
        cp=QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def apply_click(self, ot, st, it):

        try:
            t = ot.toPlainText()
            text = self.preprocessing(t)
            tr = textrank.TextRank(text)
            
            st.clear()
            it.clear()
            it.setAlignment(QtCore.Qt.AlignCenter)

            try:
                self.text_print(tr, st, it)
            except:
                traceback.print_exc()
        except:
            traceback.print_exc()
        

    def warning_click(self):

        reply = QMessageBox.question(self, "주의사항","1. 점 단위로 문장을 구분함 \n2. 문장을 넣으면 5줄로 요약",
                                     QMessageBox.Ok, QMessageBox.Close)
        
    def clear_click(self, ot, st, it):

        reply = QMessageBox.question(self, "비우기", "정말 비우실 건가요?",
                                     QMessageBox.Yes, QMessageBox.No)

        if reply == QMessageBox.Yes:
            ot.clear()
            st.clear()
            it.clear()
        else:
            pass
        

    def closeEvent(self, event):
        reply = QMessageBox.question(self, "종료", "정말 종료하실 건가요?",
                                     QMessageBox.Yes, QMessageBox.No)

        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def text_print(self, tr, st, it):

        n=1
        st.append(" ")
        for row in tr.summarize():
            st.append(str(n) + ". " + row + "\n")
            n=n+1

        keyword = ""
        for key in tr.keywords():
            keyword = keyword + key + " "
        it.append(keyword)


        
if __name__ == '__main__':

    app = QApplication(sys.argv)
    MainWidget = MainWidget()
    MainWidget.show()
    
    sys.exit(app.exec_())

