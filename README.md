# Non-literary Text summary
[![license](https://img.shields.io/badge/License-MIT-red)](https://gitlab.com/cczztczz/textmining/blob/master/LICENSE)
[![code](https://img.shields.io/badge/Code-Python3.7-blue)](https://docs.python.org/3/license.html)
[![gui](https://img.shields.io/badge/GUI-PyQT5-blue)](https://doc.qt.io/qtforpython/)
[![member](https://img.shields.io/badge/Project-Member-brightgreen)](https://gitlab.com/cczztczz/textmining/-/project_members)
[![results](https://img.shields.io/badge/Result-Program-yellow)](https://gitlab.com/cczztczz/textmining/tree/master/GUI)
[![data](https://img.shields.io/badge/Data-EBSi-blueviolet)](http://m.ebsi.co.kr/index.do)

> 국어 비문학 지문을 요약하여 수험생에게 도움을 주는 프로젝트

## :bust_in_silhouette: Participation Member

 [ 정영섭 ]  Professor  
 [ 정선경 ]  Sutudent  : [010 - 2396 - 2219]  
 [ 안성인 ]  Sutudent  : [010 - 5754 - 3027]  
 [ 한예찬 ]  Sutudent  : [010 - 9042 - 1834]  

## 📖 Introduction

 수능은 고3에게 있어서 가장 큰 관심사 중 하나입니다. 수능 응시 과목 중 국어 영역은 지문에 대한 정확한 이해와 글의 흐름을 파악 할 수 있는 능력이 요구 됩니다. 특히 국어 영역에서 독서(비문학) 파트는 지문 자체가 예술, 과학, 경제 등 수험생에게 생소하고 이해하기 어려운 글들이 대부분이고, 글의 흐름 파악도 쉽지 않습니다. 더군다나 책을 많이 읽지 않는 고등학생들에게는 독서 영역이 낯설고 어렵게만 느껴집니다.  
   
 (통계자료 : https://www.dhnews.co.kr/news/articleView.html?idxno=88159)  
   
 수험생에게 있어서 수능 문제를 푼다고 할 때 가장 큰 관심사는 바로 성적이 잘 나오는 것 입니다. 국어 영역에서 성적이 잘 나오기 위해서는 글에 대한 이해와 흐름 파악 능력이 요구됩니다. 우리는 독서 영역에서 힘들어하는 수험생들을 위하여 수능 비문학 요약 프로그램을 만들게 되었습니다.  
   
 수험생들이 문제를 풀고 나서 자신이 이해가 되지 않았던 지문을 프로그램에 넣게 되면 프로그램이 지문에서 중요한 부분만 간추려서 보여주고 중요한 단어에 대한 정보를 알려줍니다. 수험생은 오답노트를 할때 프로그램을 참고 할 수 있으며 이를 통해 수험생의 글 이해 능력과 흐름 파악 능력이 향상될 것이라고 기대합니다.

## 📂 Directory structure
``` 
  |-Data           
  |  |-Test.zip                              #비문학지문을 모아놓은 파일
  |  |-Textmining_stopword_crawling.ipynb    #한국어 불용어 사전에서 크롤링 코드
  |  |-stopword.txt                          #한국어 불용어 사전에서 크롤링해온 불용어 문서
  |
  |-Data Preprocessing    
  |  |-Textmining_지문추출.ipynb             #국어 모의고사에서 지문들만 불러온 코드
  |  |-sentences subset.ipynb                #순수 비문학 지문만 불러온 코드     
  |
  |-TextRank   
  |  |-Textming_텍스트랭크.ipynb             #TextRank 알고리즘을 적용한 코드
  |
  |-GUI                     
  |  |-summary.py                            #GUI 파일
  |  |-textrank.py                           #TextRank 알고리즘 
  |
  |-README.md                                #이 문서
```

## 💻 System requirements
기본적으로 ANACONDA 환경에 jupyter notebook에서 진행합니다.  
  
이를 위해 [ANACONDA 설치](https://www.anaconda.com/download/) 및 환경 설정이 필수적으로 필요합니다.  
  
그리고 주로 [KoNLPy](http://konlpy.org/en/latest/)를 많이 사용하는데 이를 위해 [Java 설치](https://www.java.com/ko/download/) 및 환경설정이 필요합니다. 
  
윈도우 사용자의 경우  [KoNLPy 설치](https://konlpy-ko.readthedocs.io/ko/v0.4.3/install/) 문서를 따라 설치를 하신 후 아래 내용을 진행하시길 바랍니다.


## 🌐 Dependency Build Instructions
```
pip install tika
pip install twitteR
pip install beautifulsoup4
pip install scikit-learn
pip install konlpy
pip install numpy
pip install pyqt5
pip install textrank
```


## 📝 Todo list
- 수능국어 기출문제 저장 및 비문학 지문 추출
- 지문 형태소 분석 및 Tweet기준 토큰화
- 특수문자 및 한국어 불용어 전처리
- TF-IDF 모델 생성 후  Correlation Matrix 구현
- TextRank 알고리즘 적용
- GUI환경에서 비문학 요약 프로그램 구축 




